'use strict';

// @ts-check

import { $, $$, $new } from './dom.js'
import { initialize as initialize_gravitational_lensing } from './gravitational_lensing.js'
import { initialize as initialize_time_dialation } from './time_dialation.js'


export function initialize() {
    const demos = {
        "Gravitational Lensing": initialize_gravitational_lensing,
        "Time Dialation": initialize_time_dialation
    }
    $('body').classList.add("landing-page")
    $('main').innerHTML = ''
    $('h2 button').hidden = true
    $('h2 div').innerText = "Interactive Demos"
    
    const nav = $('nav')
    nav.hidden = false
    nav.innerHTML = `
        ${Object.keys(demos).map(demo => `
            <button data-demo-name='${demo}'>${demo}</button>
        `).join('')}
        <button class="fullscreen" onclick="document.body.requestFullscreen()">Fullscreen</button>
    `
    nav.onclick = event => {
        const demo = event.target.textContent
        if (demo in demos) {
            demos[demo]()
        }
        event.stopPropagation()
    }
}

