// @ts-check

import { $, $$, $new } from './dom.js'
import { distance } from './helpers.js'

/**
 * @typedef {Object} Mass
 * @property {Number} x
 * @property {Number} y
 * @property {Number} mass
 * 
 */

/**
 * space_time_fabric_path is a function that returns a string that can be used to draw a space-time-fabric grid on an svg
 * @param {[Mass] | []} masses
 * @param {Number} width=1200 
 * @param {Number} height=700
 * @param {Number} grid_size=50
 * @returns String
 */
export function space_time_fabric_path(masses=[], width=1200, height=600, grid_size=20) {
    var grid_path = ''
    for (var x = 0; x < width; x += grid_size) {
        grid_path += `M${x} 0 v${height} `
    }
    for (var y = 0; y < height; y += grid_size) {
        grid_path += `M0 ${y} h${width} `
    }
    return grid_path
}

// 21 to 59
// 20 to 200

/** space-time curvature as a function of distance from mass
 * @param {Number} d
 * @param {Number} mass
 * @returns {Number}
 *  
 */
export function curvature(d, mass) {
    return 1 / (d * mass)
    /** reference: https://en.wikipedia.org/wiki/Gravitational_lens#Space-time_curvature
     * 
     */
}