// @ts-check

/**
 * $ is a function that returns the first element that matches the selector from the document
 * @param {string} selector 
 * @returns {HTMLElement | null}
 */
export function $(selector) {
    return document.querySelector(selector)
}

/**
 * $$ is a funtion that returns an array of elements that match the selector from the document
 * @param {string} selector
 * @returns {HTMLElement[]}
 */
export function $$(selector) {
    return Array.from(document.querySelectorAll(selector))
}

/**
 * $new is a function that creates a new element with the supplied attributes
 * @param {string} tagName
 * @param {Object} attributes
 * @returns {HTMLElement}
 * 
 */
export function $new(tagName, attributes = {}) {
    const element = document.createElement(tagName);
    Object.keys(attributes).forEach(key => {
        if (typeof attributes[key] === 'function') {
            element.addEventListener(key, attributes[key])
        } else if (typeof attributes[key] === 'object') {
            element[key] = attributes[key]
        } else {
            element.setAttribute(key, attributes[key])
        }
    })
    return element;
}