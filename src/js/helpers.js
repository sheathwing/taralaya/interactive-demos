// @ts-check

/** 
 * 
 * @typedef {[Number, Number]} Point2d
 */

/** 
 * 
 * @typedef {[Number, Number]} Range2d
 */

/**
 * 
 * @param {Point2d} client_position
 * @param {SVGElement} svg_element 
 * @returns {Point2d}
 */
export function client_to_svg_coordinate([client_x, client_y], svg_element) {
    let svg_bounding_rectangle = svg_element.getBoundingClientRect()
    let [svg_x, svg_y, svg_width, svg_height] = svg_element.getAttribute('viewBox').split(' ').map(Number)
    let x = ((svg_width / svg_bounding_rectangle.width) * (client_x - svg_bounding_rectangle.x)) + svg_x
    let y = ((svg_height / svg_bounding_rectangle.height) * (client_y - svg_bounding_rectangle.y)) + svg_y
    return [x, y]
}

/**
 * 
 * @param {Point2d} point_1 
 * @param {Point2d} point_2
 * @returns 
 */
export function distance([x1, y1], [x2, y2]) {
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))
}

/**
 * scale a range of numbers to another range of numbers
 * @param {Number} value
 * @param {Range2d} range_1
 * @param {Range2d} range_2
 * @returns {Number}
 * 
 */
export function scale(value, [min_1, max_1], [min_2, max_2]) {
    return min_2 + (max_2 - min_2) * ((value - min_1) / (max_1 - min_1))
}