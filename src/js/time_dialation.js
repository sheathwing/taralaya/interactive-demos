// @ts-check

import { $, $$, $new } from './dom.js';
import {space_time_fabric_path} from './space_time_fabric.js'
import {client_to_svg_coordinate} from './helpers.js'


export function initialize() {
    $('body').classList.remove("landing-page")
    $('nav').hidden = true
    $('h2 button').hidden = false
    $('h2 div').innerText = "Interactive Demo: Time Dialation"

    $('main').innerHTML = `
        <svg viewBox="0 0 1200 600">
            <path class="outline" d="M0 0 h1200 v600 h-1200 v-600" />
            <path id="space-time-fabric" d="${space_time_fabric_path()}" />
            
            <g transform="translate(1165, 300)">
                <text y="-110" fill="#000" stroke="none">Mass</text>
                <path d="M-20,-100 h40 v200 z" fill="#fff" stroke="#000" stroke-width="0.5"/>
                <rect id="mass_select" x="-30" width="60" height="10" rx="5" ry="5" fill="#0008" stroke="none"/>
            </g>

            <g transform="translate(600, 300)">
                <circle id="mass" class="breathe_115" r="40" fill="#000" stroke="#000" stroke-width="0.5" />
            </g>
        
        </svg>`
    
    const mass_select = $("#mass_select")
    const mass_element = $("#mass")
    const space_time_fabric = $("#space-time-fabric")
    mass_select.addEventListener('touchmove', event => {
        event.preventDefault()
        const touch = event.touches[0]
        const y = client_to_svg_coordinate([0, touch.clientY], $('main svg'))[1]
        if (y < 200) {
            y = 200
        } else if (y > 390) {
            y = 390
        }
        mass_select.setAttribute("y", y - 300)
        const new_mass = 40 - (y - 300) / 5
        mass_element.setAttribute("r", new_mass)
        space_time_fabric.setAttribute("d", space_time_fabric_path([{x: 600, y: 300, mass: new_mass}]))
    })
}

