// @ts-check

import { $, $$, $new } from './dom.js';
import {space_time_fabric_path} from './space_time_fabric.js'
import {client_to_svg_coordinate} from './helpers.js'


export function initialize() {
    $('body').classList.remove("landing-page")
    $('nav').hidden = true
    $('h2 button').hidden = false
    $('h2 div').innerText = "Interactive Demo: Gravitational Lensing"

    $('main').innerHTML = `
        <svg viewBox="0 0 1200 600">
            <path class="outline" d="M0 0 h1200 v600 h-1200 v-600" />
            <path id="space-time-fabric" d="" />
            <g transform="translate(600, 20)">
                <use class="breathe_200" href="#star" fill="#ff0" stroke="#000" stroke-width="0.5"/>
            </g>

            <g transform="translate(1165, 300)">
                <text y="-110" fill="#000" stroke="none">Mass</text>
                <path d="M-20,-100 h40 v200 z" fill="#fff" stroke="#000" stroke-width="0.5"/>
                <rect id="mass_select" class="breathe_115" x="-30" width="60" height="10" rx="5" ry="5" fill="#0008" stroke="none" transform-origin="0 0"/>
            </g>

            <g transform="translate(600, 300)">
                <circle id="mass" class="breathe_115" r="40" fill="#000" stroke="#000" stroke-width="0.5" />
            </g>

            <g fill="none" stroke="#000" stroke-width="1">
                <path id="light_path_straight" stroke-dasharray="10 10" d=""/>
                <path id="light_path" d=""/>
            </g>

            <g id="observer" transform="translate(600, 550)">
                <circle r="40" fill="#07d" stroke="#000" stroke-width="0.5" transform-origin="600 500"/>
                <text fill="#fff" stroke="#000" stroke-width="0.1">Observer</text>
            </g>
        </svg>`
    const observer = $("#observer")
    let observer_being_dragged = false
    observer.addEventListener('touchmove', event => {
        event.preventDefault()
        set_observer_x(event.touches[0].clientX)
    })
    observer.addEventListener('mousedown', event => {
        observer_being_dragged = true
    })
    observer.addEventListener('mouseup', event => {
        observer_being_dragged = false
    })
    observer.addEventListener('mousemove', event => {
        event.preventDefault()
        if (observer_being_dragged) {
            set_observer_x(event.clientX)
        }
    })

    const mass_select = $("#mass_select")
    mass_select.addEventListener('touchmove', event => {
        event.preventDefault()
        set_mass_select_y(event.touches[0].clientY)
    })
    mass_select.addEventListener('mousedown', event => {
        observer_being_dragged = true
    })
    mass_select.addEventListener('mouseup', event => {
        observer_being_dragged = false
    })
    mass_select.addEventListener('mousemove', event => {
        event.preventDefault()
        if (observer_being_dragged) {
            set_mass_select_y(event.clientY)
        }
    })

    $("#space-time-fabric").setAttribute("d", space_time_fabric_path())
    redo_light_path()
}

function set_observer_x(client_x) {
    let svg_x = client_to_svg_coordinate([client_x, 0], $('main svg'))[0]
    if (svg_x < 50) {
        svg_x = 50
    } else if (svg_x > 1150) {
        svg_x = 1150
    }
    $("#observer").setAttribute("transform", `translate(${svg_x}, 550)`)
    redo_light_path()
}

function set_mass_select_y(client_y) {
    const mass_select = $("#mass_select")
    let svg_y = client_to_svg_coordinate([0, client_y], $('main svg'))[1]
    if (svg_y < 205) {
        svg_y = 205
    } else if (svg_y > 395) {
        svg_y = 395
    }
    mass_select.setAttribute("y", svg_y - 305)
    mass_select.setAttribute("transform-origin", `0 ${svg_y - 300}`)
    const new_mass = 40 - (svg_y - 300) / 5
    $("#mass").setAttribute("r", new_mass)
    $("#space-time-fabric").setAttribute("d", space_time_fabric_path([{x: 600, y: 300, mass: new_mass}]))
    redo_light_path()
}

function redo_light_path() {
    const svg = $("main svg")
    const observer = $("#observer")
    const observer_rect = observer.getBoundingClientRect()
    const [observer_x, observer_y] = client_to_svg_coordinate([observer_rect.x + (observer_rect.width / 2), observer_rect.y + (observer_rect.height / 2)], svg)    
    console.log(`observer: {x: ${observer_x}, y: ${observer_y}}`)
    const mass_element = $("#mass")
    const mass = mass_element?.attributes["r"].value
    console.log(`mass: ${mass}`)
    const light_path = $("#light_path")
    const light_path_straight = $("#light_path_straight")

    light_path_straight.setAttribute("d", `M${observer_x},${observer_y} L600,20`)
}